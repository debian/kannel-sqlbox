kannel-sqlbox (0.7.2-5) unstable; urgency=medium

  * Update copyright info:
    + Modernize to use Format as file format identifier
      (not Format-Specification).
    + Use https protocol in protocol and source URLs.
  * Add patch 1003 to fix FTBFS with clang instead of gcc.
    Closes: Bug#742733. Thanks to Nicolas Sévelin-Radiguet.
  * Update package relations:
    + Build-depend on texlive-formats-extra (not jadetex).
      Closes: Bug#871743. Thanks to Norbert Preining.
  * Modernize autotools.
    Build-depend on automake (not automake1.11).
    Closes: Bug#865171. Thanks to Simon McVittie.
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
    + Stop build-depend on devscripts.
  * Stop track upstream source with CDBS (use gpb --uscan).
  * Add patch 1004 to allow change db type by adding db storage option.
    Closes: Bug#590692. Thanks to Arnau Rebassa and António Silva.
  * Update Vcs-* fields: Source moved to Salsa.
  * Set Rules-Requires-Root: no.
  * Declare compliance with Debian Policy 4.2.1.
  * Drop obsolete lintian override about debhelper.
  * Tighten lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 08 Oct 2018 00:37:08 +0200

kannel-sqlbox (0.7.2-4) unstable; urgency=medium

  * Update README.source to emphasize that control.in file is *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Enable support for FreeTDS.
  * Update package relations:
    + Stop build-depend on packages on behalf of kannel-dev: Corrected
      since kannel 1.4.3-1 - i.e. satisfied even in oldstable.
    + Tighten build-dependency on kannel-dev: Needed for FreeTDS
      support.
    + Relax to build-depend unversioned on cdbs devscripts debhelper:
      Needed versions satisfied even in oldstable.
    + Have kannel depend versioned on kannel-dev, and drop upper bounds
      build-dependency: Makes lock on upstream version binNMUable.
      Closes: Bug#805117. Thanks to Kurt Roeckx.
  * Update copyright info:
    + Rewrite using machine-readable format 1.0.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Declare compliance with Debian Policy 3.9.6.
  * Unfuzz patch 1001.
  * Add patch 1002 cherry-picked upstream to fix resolve kannel linkage.
  * Git-ignore quilt dir.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 23 Nov 2015 17:41:18 +0100

kannel-sqlbox (0.7.2-3) unstable; urgency=low

  * Drop locally included CDBS snippets, and local implementation of
    DEB_MAINTAINER_MODE, all included in main cdbs now.
  * Update header of rules file: Update copyright years; refer to FSF
    website (not postal address).
  * Rewrite debian/copyright using draft DEP5 rev. 135 format.
  * Tighten build-dependency on kannel-dev.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 13 Apr 2010 04:21:32 +0200

kannel-sqlbox (0.7.2-2) unstable; urgency=low

  * Use source format '3.0 (quilt)'.
  * Apply new patch 1001 fixing wrong data types. Closes: bug#565911,
    thanks to dann frazier for reporting and Travis Hansen of Gentoo for
    the patch.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 05 Feb 2010 13:02:43 +0100

kannel-sqlbox (0.7.2-1) unstable; urgency=low

  * Initial release. Closes: bug#564883.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 12 Jan 2010 16:16:37 +0100
